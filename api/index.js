import express from 'express';
import transaction from './routes/transaction';
import user from './routes/user';

const router = express.Router();

transaction(router);
user(router);

module.exports = router;
