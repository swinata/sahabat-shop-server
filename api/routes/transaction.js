import mongoose from 'mongoose';
import chalk from 'chalk';
import Transaction from '../../models/transaction';

const { log } = console;
export default function (router) {
  // Get transactions for given year and month, by userId...
  router.get('/transaction/:year/:month', (req, res) => {
    const userId = req.get('userId');
    const month = req.params.month - 1; // JS months are zero-based
    const { year } = req.params;
    const startDt = new Date(Date.UTC(year, month, 1, 0, 0, 0));
    const endDt = new Date(Date.UTC(year, month + 1, 1, 0, 0, 0));

    const qry = {
      userId,
      transactionDate: {
        $gte: startDt,
        $lt: endDt,
      },
    };

    Transaction.find(qry)
      .sort({ transactionDate: 1 })
      .exec()
      .then(docs => res.status(200)
        .json(docs))
      .catch(err => res.status(500)
        .json({
          message: 'Error finding transactions for user',
          error: err,
        }));
  });

  // Get transactions running balance for a specific user...
  router.get('/transaction/balance/:year/:month', (req, res) => {
    const userId = req.get('userId');
    const month = req.params.month - 1; // JS months are zero-based
    const { year } = req.params;
    const endDt = new Date(Date.UTC(year, month, 1));
    const pipeline = [
      {
        $match: {
          userId: mongoose.Types.ObjectId(userId),
        },
      },
      {
        $match: {
          transactionDate: { $lt: endDt },
        },
      },
      {
        $group: {
          _id: null,
          charges: { $sum: '$charge' },
          deposits: { $sum: '$deposit' },
        },
      },
    ];

    Transaction.aggregate(pipeline).exec()
      .then(docs => res.status(200)
        .json(docs))
      .catch(err => res.status(500)
        .json({
          message: 'Error finding transactions for user',
          error: err,
        }));
  });

  // Create new transaction document...
  router.post('/transaction', (req, res) => {
    const transaction = new Transaction(req.body);
    transaction.save((err) => {
      if (err) return log(chalk.blue(err));
      return res.status(200).json(transaction);
    });
  });
}
