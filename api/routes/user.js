import chalk from 'chalk';
import User from '../../models/user';

const { log } = console;
export default function (router) {
  router.get('/user/:id', (req, res) => {
    User.findById(req.params.id).exec()
      .then(docs => res.status(200)
        .json(docs))
      .catch(err => res.status(500)
        .json({
          message: 'Error finding user',
          error: err,
        }));
  });

  router.get('/user/email/:email', (req, res) => {
    User.find({ email: req.params.email }).exec()
      .then(docs => res.status(200)
        .json(docs))
      .catch(err => res.status(500)
        .json({
          message: 'Error finding Email',
          error: err,
        }));
  });

  router.post('/user', (req, res) => {
    const user = new User(req.body);
    user.save((err) => {
      if (err) return log(chalk.blue(err));
      return res.status(200).json(user);
    });
  });

  router.put('/user/:id', (req, res) => {
    log(chalk.blue(req.body));
    const qry = {
      _id: req.params.id,
    };
    const doc = {
      isActive: req.body.isActive,
    };
    User.update(qry, doc, (err, respRaw) => {
      if (err) return log(chalk.blue(err));
      return res.status(200).json(respRaw);
    });
  });
}
