const express = require('express');
// const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const chalk = require('chalk');
const api = require('./api');

const app = express();
const { log } = console;
const { error } = console;

app.set('port', (process.env.PORT || 8081));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', api);

app.use((_req, res) => {
  const err = new Error('Not Found');
  err.status = 404;
  res.json(err);
});


mongoose.connect('mongodb://swinata:Insolent01!@ds141972.mlab.com:41972/sahabat-shop', { useNewUrlParser: true });
const db = mongoose.connection;

db.on('error', error.bind(console, 'connection error:'));
db.once('open', () => {
  log(chalk.blue('Connected to database'));

  app.listen(app.get('port'), () => {
    log(chalk.blue(`API server listening on port${app.get('port')}!`));
  });
});
