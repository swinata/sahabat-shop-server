import mongoose from 'mongoose';

const { Schema } = mongoose;

const transactionSchema = new Schema({
  userId: Schema.ObjectId,
  transactionDate: { type: Date, require: true },
  transactionType: { type: String, require: true },
  description: { type: String, require: true },
  charge: { type: Number, default: 0 },
  deposit: { type: Number, default: 0 },
  notes: { type: String, require: '' },
  createdOn: { type: Date, require: Date.now },
});

const Transaction = mongoose.model('Transaction', transactionSchema);
export default Transaction;
